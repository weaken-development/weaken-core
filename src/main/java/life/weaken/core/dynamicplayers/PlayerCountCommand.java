package life.weaken.core.dynamicplayers;

import life.weaken.core.ServerDataStorage;
import life.weaken.core.utils.PlayerCountUtil;
import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;

public record PlayerCountCommand(ServerDataStorage storage) implements CommandExecutor {

    public PlayerCountCommand(ServerDataStorage storage) {
        this.storage = storage;
        PlayerCountUtil.setPlayerCount(storage.getServerData().getInt("player-count"));
    }

    @Override
    public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {
        if (args.length != 1) {
            sender.sendMessage(ChatColor.RED + "Incorrect usage: /" + label + " <amount>");
            return true;
        }

        int playercount;
        try {
            playercount = Integer.parseInt(args[0]);
        } catch (IllegalArgumentException e) {
            sender.sendMessage(ChatColor.RED + "Incorrect usage: /" + label + " <amount>");
            return true;
        }

        storage.getServerData().set("player-count", playercount);
        PlayerCountUtil.setPlayerCount(playercount);

        sender.sendMessage(ChatColor.GREEN + "Set server's max player count to " + playercount);
        return true;
    }

}
