package life.weaken.core.teleports;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.command.TabExecutor;
import org.bukkit.entity.Player;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public record TeleportCommand(TeleportManager manager) implements TabExecutor {

    @Override
    public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {
        if(!manager.teleportExists(command.getName())) {
            sender.sendMessage(ChatColor.RED + "The location for the teleport '" + command.getName() + "' is not set, please contact an admin");
            return true;
        }

        if(args.length == 0) {
            if(!(sender instanceof Player player)) {
                sender.sendMessage(ChatColor.RED + "Incorrect usage: /" + label + " <player>");
                return true;
            }
            manager.teleport(player, command.getName(), !player.hasPermission("core.teleport.instant"));
        }

        else {
            Player player = Bukkit.getPlayer(args[0]);
            if(player == null) {
                sender.sendMessage(ChatColor.RED + "That player is not online");
                return true;
            }
            manager.teleport(player, command.getName(), false);
            sender.sendMessage(ChatColor.GREEN + "Teleported " + player.getName() + " to " + command.getName());
        }
        return true;
    }

    @Override
    public List<String> onTabComplete(CommandSender sender, Command command, String label, String[] args) {
        List<String> completions = new ArrayList<>();

        if(args.length == 1) {
            completions = Bukkit.getOnlinePlayers().stream()
                    .map(Player::getName)
                    .filter(player -> player.toLowerCase().startsWith(args[0].toLowerCase()))
                    .collect(Collectors.toList());
        }

        return completions;
    }
}
