package life.weaken.core.teleports;

import life.weaken.core.ServerDataStorage;
import life.weaken.core.utils.ActionBarUtil;
import life.weaken.core.utils.CommandUtil;
import org.bukkit.ChatColor;
import org.bukkit.Location;
import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.entity.Player;
import org.bukkit.plugin.Plugin;
import org.bukkit.scheduler.BukkitRunnable;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.stream.Collectors;

public class TeleportManager {

    private final Plugin plugin;
    private final ServerDataStorage storage;

    private HashMap<String, Location> teleportMap;

    private final int teleportDelay;

    public TeleportManager(Plugin plugin, ServerDataStorage storage) {
        this.storage = storage;
        this.plugin = plugin;

        teleportMap = new HashMap<>();
        teleportDelay = plugin.getConfig().getInt("teleport-delay");
        load();
    }

    public void shutdown() {
        save();
    }

    private void load() {
        teleportMap = new HashMap<>();
        ConfigurationSection section = storage.getServerData().getConfigurationSection("teleports");
        if(section == null) return;

        for(String string : section.getKeys(false)) {
            setTeleport(string, section.getLocation(string));
        }
    }

    private void save() {
        YamlConfiguration config = storage.getServerData();
        for(String string : teleportMap.keySet()) {
            config.set("teleports." + string, teleportMap.get(string));
        }
    }

    public boolean teleportExists(String teleport) {
        return teleportMap.containsKey(teleport);
    }

    @SuppressWarnings("ConstantConditions")
    public void setTeleport(String string, Location location) {
        teleportMap.put(string, location);
        if(!CommandUtil.isCommandRegistered(string)) CommandUtil.createCommand(plugin, string).setExecutor(new TeleportCommand(this));
    }

    public void unsetTeleport(String string) {
        teleportMap.remove(string);
        if(CommandUtil.isCommandRegistered(string)) CommandUtil.deleteCommand(string);
    }

    public void teleport(Player player, String teleport, boolean cooldown) {
        if(!cooldown) {
            player.teleport(teleportMap.get(teleport));
            player.sendMessage(ChatColor.GREEN + "Teleported to " + teleport + " instantly");
            return;
        }

        player.sendMessage(ChatColor.GREEN + "Teleporting in " + (int) Math.ceil((float) teleportDelay / 20) + " seconds, don't move or your teleport will be cancelled");

        final Location preLocation = player.getLocation();
        final double preHealth = player.getHealth();
        new BukkitRunnable() {
            int counter = 0;
            @Override
            public void run() {
                if(preLocation.getWorld() != player.getWorld() ||
                        preLocation.distance(player.getLocation()) > 0.5 ||
                        preHealth < player.getHealth()) {
                    this.cancel();
                    ActionBarUtil.send(player, ChatColor.RED + "Teleport cancelled");
                    return;
                }
                if(counter >= teleportDelay) {
                    player.teleport(teleportMap.get(teleport));
                    ActionBarUtil.send(player, ChatColor.AQUA + "Teleported!");
                    this.cancel();
                    return;
                }
                ActionBarUtil.send(player, ChatColor.AQUA + "Teleporting in " + (int) Math.ceil((float) (teleportDelay - counter) / 20) + " seconds");
                counter++;
            }
        }.runTaskTimer(plugin, 0L, 1L);
    }

    public Location getTeleport(String string) {
        return teleportMap.get(string);
    }

    public List<String> getTeleports() {
        return new ArrayList<>(teleportMap.keySet());
    }

}
