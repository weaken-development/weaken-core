package life.weaken.core.teleports;

import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.command.TabExecutor;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public record UnsetTeleportCommand(TeleportManager manager) implements TabExecutor {

    @Override
    public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {
        if(args.length != 1) {
            sender.sendMessage(ChatColor.RED + "Incorrect usage: /" + label + " <teleport>");
            return true;
        }
        String teleport = args[0];
        if(!manager.teleportExists(teleport)) {
            sender.sendMessage(ChatColor.RED + "That teleport does not exist");
            return true;
        }

        manager.unsetTeleport(teleport);

        sender.sendMessage(ChatColor.GREEN + "Deleted teleport " + teleport);
        return true;
    }

    @Override
    public List<String> onTabComplete(CommandSender sender, Command command, String label, String[] args) {
        List<String> completions = new ArrayList<>();

        if(args.length == 1) {
            completions = manager.getTeleports().stream()
                    .filter(s -> s.toLowerCase().startsWith(args[0].toLowerCase()))
                    .collect(Collectors.toList());
        }

        return completions;
    }

}
