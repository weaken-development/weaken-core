package life.weaken.core.teleports;

import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.command.TabExecutor;
import org.bukkit.entity.Player;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public record SetTeleportCommand(TeleportManager manager) implements TabExecutor {

    @Override
    public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {
        if(!(sender instanceof Player player)) {
            sender.sendMessage(ChatColor.RED + "Only players can use this command");
            return true;
        }

        if(args.length != 1) {
            player.sendMessage(ChatColor.RED + "Incorrect usage: /" + label + " <teleport>");
            return true;
        }

        String teleport = args[0];
        manager.setTeleport(teleport, player.getLocation());

        player.sendMessage(ChatColor.GREEN + "Set teleport " + teleport + " to your current location");
        return true;
    }

    @Override
    public List<String> onTabComplete(CommandSender sender, Command command, String label, String[] args) {
        List<String> completions = new ArrayList<>();

        if(args.length == 1) {
            completions = manager.getTeleports().stream()
                    .filter(s -> s.toLowerCase().startsWith(args[0].toLowerCase()))
                    .collect(Collectors.toList());
        }

        return completions;
    }

}
