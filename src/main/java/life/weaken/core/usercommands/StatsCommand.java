package life.weaken.core.usercommands;

import life.weaken.core.playerdata.PlayerDataStorage;
import life.weaken.core.stats.PlayerStats;
import life.weaken.core.stats.PlayerStatsManager;
import life.weaken.core.utils.HexColorUtils;
import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.entity.Player;
import org.bukkit.plugin.Plugin;

public class StatsCommand implements CommandExecutor {

    private PlayerStatsManager manager;
    private final String PLAYER_STATS_MSG;


    public StatsCommand(Plugin plugin, PlayerStatsManager manager) {
        this.manager = manager;
        FileConfiguration config = plugin.getConfig();
        PLAYER_STATS_MSG = HexColorUtils.translateFully(config.getString("command-spy-format"));
    }


    @Override
    public boolean onCommand(CommandSender sender, Command command, String s, String[] args) {
        if(!(sender instanceof Player player)) {
            sender.sendMessage(ChatColor.RED + "This command can only be used by players");
            return true;
        }

        if (args.length <= 0){
            PlayerStats stats = manager.get(player);
            sender.sendMessage(fixVirables(
                    stats, player.getDisplayName())
            );
            return true;
        }

        String name =  args[0];

        PlayerStats stats = manager.get(name);

        if (stats == null){
            sender.sendMessage(ChatColor.RED + "This player doesn't exists");
            return true;
        }


        sender.sendMessage(fixVirables(stats, name));

        return false;
    }
    private String fixVirables(PlayerStats stats, String statsName){
        return PLAYER_STATS_MSG.replace("{NAME}", statsName)
                .replace("{KILLS}", Integer.toString(stats.getKills()))
                .replace("{DEATHS}", Integer.toString(stats.getDeaths()))
                .replace("{ACUTALY-KILLSTREAK}", Integer.toString(stats.getKills()))
                .replace("{BEST-KILLSTREAK}", Integer.toString(stats.getBestKillstreak()));
    }


}
