package life.weaken.core.tokens.storage;

import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.plugin.Plugin;
import org.mariadb.jdbc.MariaDbPoolDataSource;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.concurrent.CompletableFuture;


public class MariaDB {

    private final MariaDbPoolDataSource connectionPool;
    private final Plugin plugin;

    public MariaDB(Plugin plugin, MariaDBConfig config) {
        this.plugin = plugin;

        connectionPool = new MariaDbPoolDataSource();
        try {
            connectionPool.setServerName(config.getHost());
            connectionPool.setDatabaseName(config.getDatabase());
            connectionPool.setPort(config.getPort());
            connectionPool.setUser(config.getUsername());
            connectionPool.setPassword(config.getPassword());
            connectionPool.setMinPoolSize(8);
            connectionPool.setMaxPoolSize(32);
        } catch(SQLException e) {
            e.printStackTrace();
        }

        try(Connection connection = connectionPool.getConnection()) {
            connection.setAutoCommit(true);
            try(Statement statement = connection.createStatement()) {
                statement.execute("CREATE TABLE IF NOT EXISTS tokens(id CHAR(32) PRIMARY KEY, value INTEGER NOT NULL);");
            }
        } catch(SQLException e) {
            e.printStackTrace();
        }
    }

    private String id(Player player) {
        return player.getUniqueId().toString().replace("-", "");
    }

    public void setTokens(Player player, Integer value) {
        Bukkit.getScheduler().runTaskAsynchronously(plugin, () -> {
            try(Connection connection = connectionPool.getConnection()) {
                connection.setAutoCommit(true);
                try(Statement statement = connection.createStatement()) {
                    statement.execute("REPLACE INTO tokens(id, value) VALUES ('" + id(player) + "', " + value + ");");
                }
            } catch(SQLException e) {
                e.printStackTrace();
            }
        });
    }

    public CompletableFuture<Integer> getTokens(Player player) {
        CompletableFuture<Integer> future = new CompletableFuture<>();
        Bukkit.getScheduler().runTaskAsynchronously(plugin, () -> {
            try (Connection connection = connectionPool.getConnection()) {
                try (Statement statement = connection.createStatement()) {
                    ResultSet resultSet = statement.executeQuery("SELECT * FROM tokens WHERE id='" + id(player) + "';");
                    int tokens = 0;
                    if (resultSet.next()) tokens = resultSet.getInt("value");
                    future.complete(tokens);
                }
            } catch (SQLException e) {
                e.printStackTrace();
            }
        });
        return future;
    }

    public void shutdown() {
        connectionPool.close();
    }

}
