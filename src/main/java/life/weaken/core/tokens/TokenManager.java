package life.weaken.core.tokens;

import life.weaken.core.tokens.storage.MariaDB;
import life.weaken.core.tokens.storage.MariaDBConfig;
import org.bukkit.entity.Player;
import org.bukkit.plugin.Plugin;

import java.util.concurrent.CompletableFuture;

public class TokenManager {

    private final MariaDB mariaDB;

    private final Plugin plugin;

    public TokenManager(Plugin plugin, MariaDBConfig mariaDBConfig) {
        this.plugin = plugin;
        mariaDB = new MariaDB(plugin, mariaDBConfig);
    }

    public void shutdown() {
        mariaDB.shutdown();
    }

    public CompletableFuture<Integer> getTokens(Player player) {
        return mariaDB.getTokens(player);
    }

    public void setTokens(Player player, Integer tokens) {
        mariaDB.setTokens(player, tokens);
    }

}
