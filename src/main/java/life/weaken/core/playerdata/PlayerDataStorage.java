package life.weaken.core.playerdata;

import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.entity.Player;
import org.bukkit.plugin.Plugin;
import org.bukkit.scheduler.BukkitRunnable;
import org.bukkit.scheduler.BukkitTask;

import java.io.File;
import java.util.HashMap;

@SuppressWarnings("ResultOfMethodCallIgnored")
public class PlayerDataStorage {

    private final HashMap<Player, YamlConfiguration> dataMap;
    private final File playerDataFolder;
    private final BukkitTask saveTask;

    public PlayerDataStorage(Plugin plugin) {
        dataMap = new HashMap<>();
        playerDataFolder = new File(plugin.getDataFolder(), "playerdata");
        if(!playerDataFolder.exists()) playerDataFolder.mkdirs();

        long autosaveDelay = plugin.getConfig().getLong("playerdata-autosave-interval");

        saveTask = new BukkitRunnable() {
            @Override
            public void run() {
                saveAll();
            }
        }.runTaskTimerAsynchronously(plugin, autosaveDelay, autosaveDelay);
    }

    private File getPlayerDataFile(Player player) {
        File file = new File(playerDataFolder, player.getUniqueId() + ".yml");
        if(!file.exists()) try {
            file.createNewFile();
        } catch(Exception e) {
            e.printStackTrace();
        }
        return file;
    }

    protected void load(Player player) {
        dataMap.put(player, YamlConfiguration.loadConfiguration(getPlayerDataFile(player)));
    }

    protected void unload(Player player) {
        if(dataMap.containsKey(player)) {
            try {
                dataMap.get(player).save(getPlayerDataFile(player));
            } catch(Exception e) {
                e.printStackTrace();
            }
        }
        dataMap.remove(player);
    }

    public YamlConfiguration get(Player player) {
        if(!dataMap.containsKey(player)) load(player);
        return dataMap.get(player);
    }

    private void saveAll() {
        for(Player player : dataMap.keySet()) {
            try {
                dataMap.get(player).save(getPlayerDataFile(player));
            } catch(Exception e) {
                e.printStackTrace();
            }
        }
    }

    public void shutdown() {
        saveTask.cancel();
        saveAll();
    }

}
