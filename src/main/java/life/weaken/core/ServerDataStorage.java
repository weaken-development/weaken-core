package life.weaken.core;

import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.plugin.Plugin;
import org.bukkit.scheduler.BukkitRunnable;
import org.bukkit.scheduler.BukkitTask;

import java.io.File;

public class ServerDataStorage {

    private final Plugin plugin;

    private YamlConfiguration serverData;
    private File serverDataFile;
    private final BukkitTask serverDataSaveTask;

    public ServerDataStorage(Plugin plugin) {
        this.plugin = plugin;
        loadServerData();

        long saveDelay = plugin.getConfig().getLong("serverdata-autosave-interval");
        serverDataSaveTask = new BukkitRunnable() {
            @Override
            public void run() {
                saveServerData();
            }
        }.runTaskTimerAsynchronously(plugin, saveDelay, saveDelay);

    }

    public void shutdown() {
        serverDataSaveTask.cancel();
        saveServerData();
    }

    public YamlConfiguration getServerData() {
        return serverData;
    }

    @SuppressWarnings("ResultOfMethodCallIgnored")
    private void loadServerData() {
        serverDataFile = new File(plugin.getDataFolder(), "server-data.yml");
        if(!serverDataFile.exists()) try {serverDataFile.createNewFile();} catch(Exception e) {e.printStackTrace();}

        serverData = YamlConfiguration.loadConfiguration(serverDataFile);
    }

    private void saveServerData() {
        try {
            serverData.save(serverDataFile);
        } catch(Exception e) {
            e.printStackTrace();
        }
    }

}
