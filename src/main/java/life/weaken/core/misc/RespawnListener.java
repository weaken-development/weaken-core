package life.weaken.core.misc;

import life.weaken.core.teleports.TeleportManager;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Location;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.PlayerDeathEvent;
import org.bukkit.event.player.PlayerRespawnEvent;
import org.bukkit.plugin.Plugin;

public class RespawnListener implements Listener {

    private final boolean autoRespawn, teleportOnRespawn;
    private final Plugin plugin;
    private final TeleportManager teleportManager;

    public RespawnListener(Plugin plugin, TeleportManager teleportManager) {
        this.teleportManager = teleportManager;
        this.plugin = plugin;
        autoRespawn = plugin.getConfig().getBoolean("auto-respawn");
        teleportOnRespawn = plugin.getConfig().getBoolean("teleport-to-spawn-on-death");
    }

    @EventHandler
    public void onDeath(PlayerDeathEvent event) {
        if(!autoRespawn) return;
        Bukkit.getScheduler().runTaskLater(plugin, () -> event.getEntity().spigot().respawn(), 1L);
    }

    @EventHandler
    public void onRespawn(PlayerRespawnEvent event) {
        if(!teleportOnRespawn) return;
        Location loc = teleportManager.getTeleport("spawn");
        if(loc == null) event.getPlayer().sendMessage(ChatColor.RED + "No teleport called 'spawn' is set, please contact an admin");
        else event.setRespawnLocation(loc);
    }

}
