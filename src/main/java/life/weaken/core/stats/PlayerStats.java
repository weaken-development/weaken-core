package life.weaken.core.stats;

import life.weaken.core.playerdata.PlayerDataStorage;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.entity.Player;

import java.util.UUID;

public class PlayerStats {

    private int deaths, kills, killStreak, bestKillstreak;
    private UUID uuid;

    public PlayerStats(Player player){
        this.uuid = player.getUniqueId();
    }

    public void save(YamlConfiguration configuration){
        configuration.set("deaths", this.deaths);
        configuration.set("kills", this.kills);
        configuration.set("killStreak", this.killStreak);
        configuration.set("bestKillstreak", this.bestKillstreak);
    }


    public void breakKillstreak(){
        if (killStreak > bestKillstreak)
            bestKillstreak = killStreak;

        killStreak = 0;
    }

    public void addKillstreak(){
        killStreak++;

        if (killStreak > bestKillstreak)
            bestKillstreak = killStreak;
    }




    public void setBestKillstreak(int bestKillstreak) {
        this.bestKillstreak = bestKillstreak;
    }

    public void setDeaths(int deaths) {
        this.deaths = deaths;
    }

    public void setKills(int kills) {
        this.kills = kills;
    }

    public void setKillStreak(int killStreak) {
        this.killStreak = killStreak;
    }

    public int getBestKillstreak() {
        return bestKillstreak;
    }

    public int getDeaths() {
        return deaths;
    }

    public int getKills() {
        return kills;
    }

    public int getKillStreak() {
        return killStreak;
    }

    public UUID getUuid() {
        return uuid;
    }

}
