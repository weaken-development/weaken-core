package life.weaken.core.stats;

import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntityDamageByEntityEvent;

public record EntityDamageByEntityListener(PlayerStatsManager manager) implements Listener {


    @EventHandler
    public void onKill(EntityDamageByEntityEvent event){
        if (!(event.getEntity() instanceof Player) || !(event.getDamager() instanceof Player)) return;

        Player player = (Player) event.getEntity();
        Player atacker = (Player) event.getDamager();



        if (player.getHealth() - event.getFinalDamage() > 0) return;


        PlayerStats playerStats = manager.get(player);
        PlayerStats atackerStats = manager.get(atacker);

        playerStats.setDeaths(playerStats.getDeaths()+1);
        playerStats.breakKillstreak();

        atackerStats.setKills(atackerStats.getKills()+1);
        atackerStats.addKillstreak();

        manager.save(player);
        manager.save(atacker);


    }




}
