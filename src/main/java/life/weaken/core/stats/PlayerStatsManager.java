package life.weaken.core.stats;

import life.weaken.core.playerdata.PlayerDataStorage;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.plugin.Plugin;

import java.util.HashMap;
import java.util.UUID;

public class PlayerStatsManager {


    private final PlayerDataStorage storage;

    private final HashMap<UUID, PlayerStats> stats = new HashMap<>();


    public PlayerStatsManager(PlayerDataStorage storage){
        this.storage = storage;
    }

    public void save(Player player){
        PlayerStats stats = get(player);

        stats.save(storage.get(player));
    }


    public PlayerStats get(String name){
        UUID uuid = Bukkit.getOfflinePlayer(name).getUniqueId();

        return stats.get(uuid);
    }

    public PlayerStats get(Player player){
        PlayerStats playerStats = stats.get(player.getUniqueId());
        if (playerStats == null)
            playerStats = create(player);

        return playerStats;
    }

    private PlayerStats create(Player player){
        if (stats.containsKey(player.getUniqueId())) return stats.get(player.getUniqueId());

        PlayerStats playerStats = new PlayerStats(player);

        stats.put(player.getUniqueId(), playerStats);

        return playerStats;
    }



}
