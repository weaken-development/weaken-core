package life.weaken.core.chat;

public class ChatException extends Exception{

    public static class FilteredMessage extends ChatException {}

}
