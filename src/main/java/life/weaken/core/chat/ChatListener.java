package life.weaken.core.chat;

import org.bukkit.ChatColor;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.AsyncPlayerChatEvent;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.event.player.PlayerQuitEvent;

public record ChatListener(ChatManager manager) implements Listener {

    @EventHandler
    public void onChat(AsyncPlayerChatEvent event) {
        if(manager.isChatMuted() && !event.getPlayer().hasPermission("core.mutechat.bypass")) {
            event.setCancelled(true);
            event.getPlayer().sendMessage(ChatColor.RED + "You can not speak now, the chat is currently muted.");
            return;
        }

        try {
            event.setFormat(manager.formatMessage(event.getPlayer(), event.getMessage()));
        }
        catch(ChatException.FilteredMessage e) {
            event.setCancelled(true);
            event.getPlayer().sendMessage(ChatColor.RED + "You used a filtered word in your message! Please refrain from doing this");
        }
    }

    @EventHandler
    public void onJoin(PlayerJoinEvent event) {
        event.setJoinMessage(manager.formatJoin(event.getPlayer()));
    }

    @EventHandler
    public void onQuit(PlayerQuitEvent event) {
        event.setQuitMessage(manager.formatLeave(event.getPlayer()));
    }

}
