package life.weaken.core.chat;

import org.bukkit.configuration.file.FileConfiguration;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Objects;

public class ChatFilter {

    private final List<String> filtered;
    private final List<AlikePair> alikePairs;


    public ChatFilter(FileConfiguration config) {
        filtered = config.getStringList("chat.filter");
        alikePairs = new ArrayList<>();
        alikePairs.add(new AlikePair('0', 'o'));
        alikePairs.add(new AlikePair('@', 'a', '4'));
        alikePairs.add(new AlikePair('7', 't'));
        alikePairs.add(new AlikePair('5', 's'));
        alikePairs.add(new AlikePair('3', 'e'));
        alikePairs.add(new AlikePair('i', 'l', '|', ';', ':'));
        alikePairs.add(new AlikePair('q', 'g'));
    }

    private boolean containsAlike(String message, String word) {
        message = message.toLowerCase();
        word = word.toLowerCase();
        for(int i = 0; i <= message.length()-word.length(); i++) {
            int same = 0;
            for(int x = 0; x < word.length(); x++) {
                if(!areAlike(message.charAt(i + x), word.charAt(x))) break;
                same++;
            }
            if(same == word.length()) return true;
        }
        return false;
    }

    public boolean isBad(String message) {
        for(String word : filtered) if(containsAlike(message, word)) return true;
        return false;
    }

    private boolean areAlike(Character c1, Character c2) {
        return alikePairFor(c1).equals(alikePairFor(c2));
    }

    private AlikePair alikePairFor(Character c) {
        for(AlikePair pair : alikePairs) if(pair.getChars().contains(c)) return pair;
        return new AlikePair(c);
    }

    private class AlikePair {
        private final List<Character> strings;
        public AlikePair(Character... chars) {
            strings = Arrays.asList(chars);
        }
        public List<Character> getChars() {
            return strings;
        }

        @Override
        public boolean equals(Object o) {
            if (this == o) return true;
            if (o == null || getClass() != o.getClass()) return false;
            AlikePair alikePair = (AlikePair) o;
            return Objects.equals(strings, alikePair.strings);
        }
    }
}
