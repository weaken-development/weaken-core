package life.weaken.core.chat.commands;

import life.weaken.core.chat.ChatManager;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;

public record MuteChatCommand(ChatManager manager) implements CommandExecutor {

    @Override
    public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {
        boolean muted = !manager.isChatMuted();
        manager.setChatMuted(muted);

        if(muted) Bukkit.broadcastMessage(ChatColor.GREEN + "Chat has been muted by " + sender.getName());
        else Bukkit.broadcastMessage(ChatColor.GREEN + "Chat has been unmuted by " + sender.getName());

        return true;
    }

}
