package life.weaken.core.chat.commands;

import life.weaken.core.chat.ChatManager;
import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;

public record ClearChatCommand(ChatManager manager) implements CommandExecutor {

    @Override
    public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {
        manager.clearChat(ChatColor.GREEN + "Chat has been cleared by " + sender.getName());
        return true;
    }
}
