package life.weaken.core.chat.commands;

import life.weaken.core.chat.ChatManager;
import life.weaken.core.utils.HexColorUtils;
import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.command.TabExecutor;
import org.bukkit.entity.Player;

import java.util.ArrayList;
import java.util.List;

public record RankColorCommand(ChatManager chatManager) implements TabExecutor {

    @Override
    public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {
        if(!(sender instanceof Player player)) {
            sender.sendMessage(ChatColor.RED + "This command can only be used by players");
            return true;
        }

        if(args.length != 3) {
            sender.sendMessage(ChatColor.RED + "Incorrect usage: /" + label + " <r> <g> <b>");
            return true;
        }

        int r, g, b;
        try {
            r = Integer.parseInt(args[0]);
            g = Integer.parseInt(args[1]);
            b = Integer.parseInt(args[2]);
        } catch(IllegalArgumentException e) {
            sender.sendMessage(ChatColor.RED + "Incorrect usage: /" + label + " <r> <g> <b>");
            return true;
        }

        if(r > 255 || g > 255 || b > 255 || r < 0 || g < 0 || b < 0) {
            sender.sendMessage(ChatColor.RED + "Colors can't be higher than 255 or lower than 0");
            return true;
        }

        chatManager.setRankColor(player, r, g, b);
        sender.sendMessage(ChatColor.GREEN + "Set your rank color to " + r + ", " + g + ", " + b + " " +
                HexColorUtils.translateFully(chatManager.getRankColor(player)) + "this");
        return true;
    }

    @Override
    public List<String> onTabComplete(CommandSender sender, Command command, String label, String[] args) {
        List<String> completions = new ArrayList<>();
        if(args.length < 4 && args[args.length - 1].equals("")) completions.add("0");
        return completions;
    }
}
