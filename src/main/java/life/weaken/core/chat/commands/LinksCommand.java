package life.weaken.core.chat.commands;

import life.weaken.core.chat.ChatManager;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;

public record LinksCommand(ChatManager manager) implements CommandExecutor {

    @Override
    public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {
        sender.sendMessage(manager.getLinksText());
        return true;
    }

}
