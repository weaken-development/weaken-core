package life.weaken.core.chat;

import life.weaken.core.playerdata.PlayerDataStorage;
import life.weaken.core.utils.HexColorUtils;
import net.luckperms.api.LuckPerms;
import net.luckperms.api.model.user.User;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Sound;
import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.entity.Player;
import org.bukkit.plugin.Plugin;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.regex.Pattern;

public class ChatManager {
    private final String JOIN_FORMAT, LEAVE_FORMAT, LINKS_TEXT;
    private final Map<String, String> groupFormatMap;

    private final LuckPerms lpApi;
    private final Plugin plugin;
    private final ChatFilter chatFilter;

    private boolean muted;
    private final PlayerDataStorage playerDataManager;

    public ChatManager(Plugin plugin, LuckPerms lpApi, PlayerDataStorage pdm) {
        playerDataManager = pdm;

        FileConfiguration config = plugin.getConfig();
        this.plugin = plugin;
        this.lpApi = lpApi;
        this.chatFilter = new ChatFilter(config);

        muted = false;


        JOIN_FORMAT = HexColorUtils.translateFully(config.getString("chat.join-message"));
        LEAVE_FORMAT = HexColorUtils.translateFully(config.getString("chat.leave-message"));
        LINKS_TEXT = HexColorUtils.translateFully(String.join("\n", config.getStringList("chat.links")));
        groupFormatMap = new HashMap<>();

        ConfigurationSection groupFormatSection = config.getConfigurationSection("chat.formats");
        if(groupFormatSection != null) for(String group : groupFormatSection.getKeys(false))
            groupFormatMap.put(group, HexColorUtils.translateFully(groupFormatSection.getString(group)));
    }

    public String getRankColor(Player player) {
        String color = playerDataManager.get(player).getString("rank-color");
        if(color == null || color.length() == 0) color = "#ffffff";
        return color;
    }

    public void setRankColor(Player player, int r, int g, int b) {
        String color = "#" + HexColorUtils.toHexString(r) + HexColorUtils.toHexString(g) + HexColorUtils.toHexString(b);
        playerDataManager.get(player).set("rank-color", color);
    }

    public String formatJoin(Player player) {
        return JOIN_FORMAT.replace("{NAME}", player.getName());
    }

    public String formatLeave(Player player) {
        return LEAVE_FORMAT.replace("{NAME}", player.getName());
    }

    public String formatMessage(Player player, String message) throws ChatException.FilteredMessage {
        if(chatFilter.isBad(message)) throw new ChatException.FilteredMessage();

        User user = lpApi.getUserManager().getUser(player.getUniqueId());
        if(user == null) return ChatColor.WHITE + player.getName() + ChatColor.GRAY + " » " + ChatColor.WHITE + message;

        ArrayList<String> components = new ArrayList<>();

        String groupFromat = groupFormatMap.get(user.getPrimaryGroup());
        if(groupFromat.contains("{RANKCOLOR}")) groupFromat = groupFromat.replace("{RANKCOLOR}", HexColorUtils.translateFully(getRankColor(player)));
        components.add(groupFromat);

        components.add(ChatColor.WHITE + player.getName());
        components.add(ChatColor.GRAY + "»");

        if(player.hasPermission("core.colorchat")) message = HexColorUtils.translateFully(message);

        for(Player p : Bukkit.getOnlinePlayers()) if(player.canSee(p) && ChatColor.stripColor(message).toLowerCase().contains(p.getName().toLowerCase())) {
            message = message.replaceAll("(?i)" + Pattern.quote(p.getName()), ChatColor.AQUA + "" + ChatColor.ITALIC + "@" + p.getName() + ChatColor.WHITE);
            p.playSound(p.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1, 1);
        }
        components.add(ChatColor.WHITE + message);

        return String.join(" ", components).replace("%", "%%");
    }

    public boolean isChatMuted() {
        return muted;
    }

    public void setChatMuted(boolean b) {
        muted = b;
    }

    public void clearChat(String... finalMessages) {
        for(Player player : Bukkit.getOnlinePlayers()) {
            Bukkit.getScheduler().runTaskAsynchronously(plugin, () -> {
                for(int i = 0; i < 500; i++) {
                    player.sendMessage(" ");
                    player.sendMessage("  ");
                }
                if(finalMessages.length > 0) player.sendMessage(finalMessages);
            });
        }
    }

    public String getLinksText() {
        return LINKS_TEXT;
    }

}
