package life.weaken.core.admincommands;

import life.weaken.core.playerdata.PlayerDataStorage;
import life.weaken.core.utils.HexColorUtils;
import org.bukkit.Bukkit;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerCommandPreprocessEvent;
import org.bukkit.plugin.Plugin;

public class AdminCommandsListener implements Listener {

    private final PlayerDataStorage storage;

    private final String CSPY_MSG;

    public AdminCommandsListener(Plugin plugin, PlayerDataStorage storage) {
        this.storage = storage;

        FileConfiguration config = plugin.getConfig();
        CSPY_MSG = HexColorUtils.translateFully(config.getString("command-spy-format"));
    }

    @EventHandler
    public void onCommand(PlayerCommandPreprocessEvent event) {
        for(Player player : Bukkit.getOnlinePlayers()) {
            if(player.hasPermission("core.commandspy") && storage.get(player).getBoolean("command-spy"))
                player.sendMessage(CSPY_MSG
                        .replace("{PLAYER}", event.getPlayer().getName())
                        .replace("{COMMAND}", event.getMessage()));
        }
    }

}
