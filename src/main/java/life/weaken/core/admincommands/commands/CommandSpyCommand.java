package life.weaken.core.admincommands.commands;

import life.weaken.core.playerdata.PlayerDataStorage;
import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.entity.Player;

public record CommandSpyCommand(PlayerDataStorage storage) implements CommandExecutor {

    @Override
    public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {
        if(!(sender instanceof Player player)) {
            sender.sendMessage(ChatColor.RED + "This command can only be used by players");
            return true;
        }

        YamlConfiguration config = storage.get(player);

        boolean cspy = !config.getBoolean("command-spy");
        config.set("command-spy", cspy);

        sender.sendMessage(ChatColor.GREEN + "Command spy has been " + (cspy ? "enabled" : "disabled"));
        return true;
    }

}
