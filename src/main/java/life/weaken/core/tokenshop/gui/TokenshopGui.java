package life.weaken.core.tokenshop.gui;

import life.weaken.core.gui.GuiManager;
import life.weaken.core.gui.guis.Gui;
import life.weaken.core.tokenshop.TokenshopManager;
import life.weaken.core.utils.ItemBuilder;
import org.bukkit.Material;

public class TokenshopGui extends Gui {

    private final TokenshopManager tokenManager;

    public TokenshopGui(GuiManager a, TokenshopManager manager, int rows) {
        super(a, "Tokenshop", rows);
        tokenManager = manager;




        fillRemaining(new ItemBuilder(Material.BLACK_STAINED_GLASS_PANE).setName(" ").create());
    }

}
