package life.weaken.core.tokenshop.gui;

import life.weaken.core.gui.guis.Gui;
import life.weaken.core.gui.items.GuiItem;
import life.weaken.core.tokens.TokenManager;
import life.weaken.core.tokenshop.items.Tokenshop;
import life.weaken.core.utils.FutureUtils;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Sound;
import org.bukkit.entity.Player;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.plugin.Plugin;

import java.util.concurrent.Future;

public class TokenshopGuiItem extends GuiItem {

    private final Tokenshop tokenshop;
    private final TokenManager tokenManager;
    private final Plugin plugin;

    public TokenshopGuiItem(Gui gui, Plugin plugin, TokenManager tokenManager, Tokenshop tokenshop) {
        super(gui, tokenshop.getDisplay());
        this.tokenshop = tokenshop;
        this.tokenManager = tokenManager;
        this.plugin = plugin;
    }

    @SuppressWarnings("ConstantConditions")
    @Override
    public void click(InventoryClickEvent event) {
        event.setCancelled(true);

        Player player = (Player) event.getWhoClicked();
        Future<Integer> tokensFuture = tokenManager.getTokens(player);

        player.closeInventory();

        FutureUtils.waitForFuture(plugin, () -> {
            int tokens = FutureUtils.get(tokensFuture);

            if(tokens >= tokenshop.getCost()) {
                tokenManager.setTokens(player, tokens - tokenshop.getCost());
                Bukkit.getScheduler().runTask(getGui().getGuiManager().getPlugin(), () -> buy(player));
                player.sendMessage(ChatColor.GREEN + "Purchase successful!");
                player.playSound(player.getLocation(), Sound.BLOCK_NOTE_BLOCK_PLING, 1, 2);
            } else {
                player.sendMessage(ChatColor.RED + "You don't have enough tokens to buy that");
            }
        }, tokensFuture);
    }

    public void buy(Player player) {
        tokenshop.buy(player);
    }

}
