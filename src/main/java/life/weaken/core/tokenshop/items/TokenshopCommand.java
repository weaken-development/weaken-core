package life.weaken.core.tokenshop.items;

import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;

public class TokenshopCommand extends Tokenshop {

    private final String command;

    public TokenshopCommand(ItemStack display, int cost, String command) {
        super(display, cost);
        this.command = command;
    }

    @Override
    public void buy(Player player) {
        Bukkit.dispatchCommand(Bukkit.getConsoleSender(), command.replace("{PLAYER}", player.getName()));
    }

}
