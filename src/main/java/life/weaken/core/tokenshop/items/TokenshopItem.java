package life.weaken.core.tokenshop.items;

import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;

public class TokenshopItem extends Tokenshop {

    private final ItemStack item;

    public TokenshopItem(ItemStack display, int cost, ItemStack item) {
        super(display, cost);
        this.item = item;
    }

    @Override
    public void buy(Player player) {
        player.getInventory().addItem(item.clone());
    }

}
