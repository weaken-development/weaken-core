package life.weaken.core.tokenshop.items;

import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;

public abstract class Tokenshop {

    private ItemStack display;
    private int cost;

    public Tokenshop(ItemStack display, int cost) {
        this.display = display;
        this.cost = cost;
    }

    public abstract void buy(Player player);

    public int getCost() {
        return cost;
    }

    public ItemStack getDisplay() {
        return display;
    }

}
