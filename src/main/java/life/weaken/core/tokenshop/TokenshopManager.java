package life.weaken.core.tokenshop;

import life.weaken.core.tokens.TokenManager;
import life.weaken.core.tokenshop.items.Tokenshop;
import life.weaken.core.tokenshop.items.TokenshopItem;
import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.plugin.Plugin;

import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;

public class TokenshopManager {

    private final List<Tokenshop> shops;

    @SuppressWarnings("ConstantConditions")
    public TokenshopManager(Plugin plugin, TokenManager tokenManager) {
        shops = new ArrayList<>();

        ConfigurationSection tokenshop = plugin.getConfig().getConfigurationSection("tokenshop.items");

        for(String s : tokenshop.getKeys(false)) {
            String type = tokenshop.getString(s + ".type");

            if(type.equalsIgnoreCase("command")) {

            }
            else if(type.equalsIgnoreCase("item")) {

            } else {
                plugin.getLogger().log(Level.SEVERE, "Malformed tokenshop type (" + s + ", " + type + "), skipping...");
            }

        }

    }




}
