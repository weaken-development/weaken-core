package life.weaken.core.utils;

import org.bukkit.plugin.Plugin;
import org.bukkit.scheduler.BukkitRunnable;

import java.util.concurrent.Future;

public class FutureUtils {

    public static void waitForFuture(Plugin plugin, Runnable runnable, Future<?> future) {
        new BukkitRunnable() {
            @Override
            public void run() {
                if(future.isCancelled()) this.cancel();
                if(future.isDone()) {
                    runnable.run();
                    this.cancel();
                }
            }
        }.runTaskTimerAsynchronously(plugin, 0L, 1L);
    }

    public static void waitForFutures(Plugin plugin, Runnable runnable, Future<?>... futures) {
        new BukkitRunnable() {
            @Override
            public void run() {
                for(Future<?> future : futures) {
                    if(!future.isDone()) {
                        return;
                    }
                }
                runnable.run();
                this.cancel();
            }
        }.runTaskTimerAsynchronously(plugin, 0L, 1L);
    }

    public static <K> K get(Future<K> future) {
        try {
            return future.get();
        } catch(Exception e) {
            e.printStackTrace();
        }
        return null;
    }


}
