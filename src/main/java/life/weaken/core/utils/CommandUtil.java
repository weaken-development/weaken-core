package life.weaken.core.utils;

import org.bukkit.Bukkit;
import org.bukkit.command.*;
import org.bukkit.plugin.Plugin;
import org.bukkit.plugin.SimplePluginManager;

import java.lang.reflect.Constructor;
import java.lang.reflect.Field;
import java.util.Map;

public class CommandUtil {

    public static PluginCommand createCommand(Plugin plugin, String commandName) {
        try {
            Constructor<PluginCommand> constructor = PluginCommand.class.getDeclaredConstructor(String.class, Plugin.class);
            constructor.setAccessible(true);
            PluginCommand command = constructor.newInstance(commandName, plugin);

            Field commandMap = SimplePluginManager.class.getDeclaredField("commandMap");
            commandMap.setAccessible(true);
            CommandMap map = (CommandMap) commandMap.get(Bukkit.getPluginManager());
            map.register("weaken-core", command);
            return command;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    @SuppressWarnings("unchecked")
    public static void deleteCommand(String commandName) {
        try {
            Constructor<PluginCommand> constructor = PluginCommand.class.getDeclaredConstructor(String.class, Plugin.class);
            constructor.setAccessible(true);

            Field commandMap = SimplePluginManager.class.getDeclaredField("commandMap");
            commandMap.setAccessible(true);
            SimpleCommandMap map = (SimpleCommandMap) commandMap.get(Bukkit.getPluginManager());

            Field knownCommandsField = SimpleCommandMap.class.getDeclaredField("knownCommands");
            knownCommandsField.setAccessible(true);
            Map<String, Command> knownCommands = (Map<String, Command>) knownCommandsField.get(map);

            String key = null;
            for(Map.Entry<String, Command> entry : knownCommands.entrySet()) {
                if(entry.getValue().getName().equalsIgnoreCase(commandName)) key = entry.getKey();
            }
            knownCommands.remove(key);

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @SuppressWarnings("unchecked")
    public static boolean isCommandRegistered(String commandName) {
        try {
            Constructor<PluginCommand> constructor = PluginCommand.class.getDeclaredConstructor(String.class, Plugin.class);
            constructor.setAccessible(true);

            Field commandMap = SimplePluginManager.class.getDeclaredField("commandMap");
            commandMap.setAccessible(true);
            SimpleCommandMap map = (SimpleCommandMap) commandMap.get(Bukkit.getPluginManager());

            Field knownCommandsField = SimpleCommandMap.class.getDeclaredField("knownCommands");
            knownCommandsField.setAccessible(true);
            Map<String, Command> knownCommands = (Map<String, Command>) knownCommandsField.get(map);

            for(Command command : knownCommands.values()) {
                if (command.getName().equalsIgnoreCase(commandName)) return true;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return false;
    }

}
