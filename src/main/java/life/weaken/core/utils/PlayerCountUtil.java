package life.weaken.core.utils;

import net.minecraft.server.MinecraftServer;
import net.minecraft.server.players.PlayerList;
import org.bukkit.Bukkit;
import org.bukkit.craftbukkit.v1_17_R1.CraftServer;

import java.lang.reflect.Field;

public class PlayerCountUtil {

    private static Field dedicatedServerField, playerListField, playerCountField;

    private static Field getField(Class<?> clazz, String fieldName) {
        try {
            Field field = clazz.getDeclaredField(fieldName);
            field.setAccessible(true);
            return field;
        } catch(Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    private static Object getFieldValue(Field field, Object object) {
        try {
            return field.get(object);
        } catch(Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    private static void setFieldValue(Field field, Object object,  Object value) {
        try {
            field.set(object, value);
        } catch(Exception e) {
            e.printStackTrace();
        }
    }

    public static void init() {
        dedicatedServerField = getField(CraftServer.class, "console");
        playerListField = getField(MinecraftServer.class, "S");
        playerCountField = getField(PlayerList.class, "f");
    }

    private static MinecraftServer getMinecraftServer() {
        return (MinecraftServer) getFieldValue(dedicatedServerField, Bukkit.getServer());
    }

    private static PlayerList getPlayerList() {
        return (PlayerList) getFieldValue(playerListField, getMinecraftServer());
    }

    public static void setPlayerCount(int playerCount) {
        setFieldValue(playerCountField, getPlayerList(), playerCount);
    }

}
