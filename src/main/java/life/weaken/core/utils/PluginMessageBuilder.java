package life.weaken.core.utils;

import com.google.common.io.ByteArrayDataOutput;
import com.google.common.io.ByteStreams;

public class PluginMessageBuilder {

    private ByteArrayDataOutput data;

    public PluginMessageBuilder() {
        data = ByteStreams.newDataOutput();
    }

    public PluginMessageBuilder write(String string) {
        data.writeUTF(string);
        return this;
    }

    public PluginMessageBuilder writeShort(int i) {
        data.writeShort(i);
        return this;
    }

    public PluginMessageBuilder writeInt(int i) {
        data.writeInt(i);
        return this;
    }

    public PluginMessageBuilder writeBytes(byte[] bytes) {
        data.write(bytes);
        return this;
    }

    public byte[] toByteArray() {
        return data.toByteArray();
    }


}
