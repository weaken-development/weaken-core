package life.weaken.core.utils;

import org.bukkit.Material;

public class MaterialUtils {

    public static Material fromString(String string) {
        try {
            return Material.valueOf(string.toUpperCase());
        } catch(IllegalArgumentException e) {
            return null;
        }
    }

}
