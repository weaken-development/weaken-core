package life.weaken.core;

import life.weaken.core.admincommands.AdminCommandsListener;
import life.weaken.core.admincommands.commands.CommandSpyCommand;
import life.weaken.core.bungeeapi.BungeeAPI;
import life.weaken.core.bungeeapi.redis.RedisConfig;
import life.weaken.core.chat.ChatListener;
import life.weaken.core.chat.ChatManager;
import life.weaken.core.chat.commands.*;
import life.weaken.core.dynamicplayers.PlayerCountCommand;
import life.weaken.core.gui.GuiListener;
import life.weaken.core.gui.GuiManager;
import life.weaken.core.misc.RespawnListener;
import life.weaken.core.playerdata.PlayerDataListener;
import life.weaken.core.playerdata.PlayerDataStorage;
import life.weaken.core.stats.EntityDamageByEntityListener;
import life.weaken.core.stats.PlayerStatsManager;
import life.weaken.core.teleports.SetTeleportCommand;
import life.weaken.core.teleports.TeleportManager;
import life.weaken.core.teleports.UnsetTeleportCommand;
import life.weaken.core.tokens.TokenManager;
import life.weaken.core.tokens.storage.MariaDBConfig;
import life.weaken.core.usercommands.StatsCommand;
import life.weaken.core.utils.PlayerCountUtil;
import net.luckperms.api.LuckPermsProvider;
import org.bukkit.Bukkit;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.plugin.Plugin;
import org.bukkit.plugin.PluginManager;
import org.bukkit.plugin.java.JavaPlugin;

import java.util.logging.Level;


public class CorePlugin extends JavaPlugin {

    private PlayerDataStorage playerDataStorage;
    private ServerDataStorage serverDataStorage;

    private BungeeAPI bungeeAPI;
    private ChatManager chatManager;
    private TokenManager tokenManager;
    private GuiManager guiManager;
    private TeleportManager teleportManager;
    private PlayerStatsManager playerStatsManager;


    @Override
    public void onEnable() {
        PlayerCountUtil.init();
        grabDependency("LuckPerms");

        saveDefaultConfig();
        FileConfiguration config = getConfig();

        RedisConfig redisConfig = new RedisConfig(config.getString("server"),
                config.getString("redis.host"),
                config.getInt("redis.port"),
                config.getString("redis.username"),
                config.getString("redis.password"));

        MariaDBConfig mariaDBConfig = new MariaDBConfig(config.getString("mariadb.host"),
                config.getInt("mariadb.port"),
                config.getString("mariadb.username"),
                config.getString("mariadb.password"),
                config.getString("mariadb.database"));

        playerDataStorage = new PlayerDataStorage(this);
        serverDataStorage = new ServerDataStorage(this);

        tokenManager = new TokenManager(this, mariaDBConfig);
        chatManager = new ChatManager(this, LuckPermsProvider.get(), playerDataStorage);
        bungeeAPI = new BungeeAPI(this, redisConfig);
        guiManager = new GuiManager(this);
        teleportManager = new TeleportManager(this, serverDataStorage);
        playerStatsManager = new PlayerStatsManager(playerDataStorage);


        PluginManager pluginManager = Bukkit.getPluginManager();
        pluginManager.registerEvents(new PlayerDataListener(playerDataStorage), this);
        pluginManager.registerEvents(new GuiListener(guiManager), this);
        pluginManager.registerEvents(new ChatListener(chatManager), this);
        pluginManager.registerEvents(new AdminCommandsListener(this, playerDataStorage), this);
        pluginManager.registerEvents(new RespawnListener(this, teleportManager), this);
        pluginManager.registerEvents(new EntityDamageByEntityListener(playerStatsManager), this);

        registerCommands();
    }

    @Override
    public void onDisable() {
        bungeeAPI.shutdown();
        tokenManager.shutdown();
        guiManager.shutdown();
        teleportManager.shutdown();

        serverDataStorage.shutdown();
        playerDataStorage.shutdown();
    }

    @SuppressWarnings("ConstantConditions")
    public void registerCommands() {
        getCommand("mutechat").setExecutor(new MuteChatCommand(chatManager));
        getCommand("clearchat").setExecutor(new ClearChatCommand(chatManager));
        getCommand("links").setExecutor(new LinksCommand(chatManager));
        getCommand("broadcast").setExecutor(new BroadcastCommand());
        getCommand("rankcolor").setExecutor(new RankColorCommand(chatManager));

        getCommand("playercount").setExecutor(new PlayerCountCommand(serverDataStorage));

        getCommand("setteleport").setExecutor(new SetTeleportCommand(teleportManager));
        getCommand("unsetteleport").setExecutor(new UnsetTeleportCommand(teleportManager));

        getCommand("commandspy").setExecutor(new CommandSpyCommand(playerDataStorage));

        getCommand("stats").setExecutor(new StatsCommand(this, playerStatsManager));
    }

    private Plugin grabDependency(String name) {
        Plugin plugin = Bukkit.getPluginManager().getPlugin(name);
        if(plugin == null || !plugin.isEnabled()) {
            getLogger().log(Level.SEVERE, "Could not find " + name + ", disabling...");
            getServer().getPluginManager().disablePlugin(this);
        }
        return plugin;
    }

    public BungeeAPI getBungeeAPI() {
        return bungeeAPI;
    }
    public TokenManager getTokenManager() {
        return tokenManager;
    }
    public GuiManager getGuiManager() {
        return guiManager;
    }
    public PlayerDataStorage getPlayerDataStorage() {
        return playerDataStorage;
    }
    public ServerDataStorage getServerDataStorage() {
        return serverDataStorage;
    }

}
