package life.weaken.core.bungeeapi;

import life.weaken.core.bungeeapi.pluginmessage.BungeeBuiltin;
import life.weaken.core.bungeeapi.redis.Redis;
import life.weaken.core.bungeeapi.redis.RedisConfig;
import org.bukkit.entity.Player;
import org.bukkit.plugin.Plugin;

import java.util.List;
import java.util.concurrent.Future;

public class BungeeAPI {

    private final Plugin plugin;

    private BungeeBuiltin bungeeBuiltin;
    private Redis redis;

    public BungeeAPI(Plugin plugin, RedisConfig redisConfig) {
        this.plugin = plugin;
        this.bungeeBuiltin = new BungeeBuiltin(plugin);
        this.redis = new Redis(plugin, redisConfig);
    }

    public void shutdown() {
        redis.shutdown();
        bungeeBuiltin.shutdown();

        bungeeBuiltin = null;
        redis = null;
    }

    public void kick(String player, String reason) {
        if(bungeeBuiltin == null) return;
        bungeeBuiltin.kickPlayer(player, reason);
    }

    public void kick(Player player, String reason) {
        if(bungeeBuiltin == null) return;
        kick(player.getName(), reason);
    }

    public void connect(Player player, String server) {
        if(bungeeBuiltin == null) return;
        bungeeBuiltin.connect(player, server);
    }

    public void connect(String player, String server) {
        if(bungeeBuiltin == null) return;
        bungeeBuiltin.connectOther(player, server);
    }

    public void message(String player, String message) {
        if(bungeeBuiltin == null) return;
        bungeeBuiltin.message(player, message);
    }

    public void message(Player player, String message) {
        if(bungeeBuiltin == null) return;
        message(player.getName(), message);
    }

    public void broadcast(String message) {
        if(bungeeBuiltin == null) return;
        bungeeBuiltin.message(message);
    }

    public Future<List<String>> getPlayerList(String server) {
        if(bungeeBuiltin == null) return null;
        return bungeeBuiltin.playerList(server);
    }

    public Future<String> getServerName() {
        if(bungeeBuiltin == null) return null;
        return bungeeBuiltin.getServer();
    }

    public Future<String> getServerStatus(String server) {
        if(redis == null) return null;
        return redis.getStatus(server);
    }

    public Future<Integer> getMaxPlayers(String server) {
        if(redis == null) return null;
        return redis.getMaxPlayerCount(server);
    }

    public Future<Long> getLastRedisUpdate(String server) {
        if(redis == null) return null;
        return redis.getLastUpdate(server);
    }

    public Future<Integer> getPlayerCount(String server) {
        if(redis == null) return null;
        return redis.getPlayerCount(server);
    }

    public Future<Integer> getGlobalPlayerCount() {
        if(redis == null) return null;
        return redis.getGlobalPlayerCount();
    }

    public Future<List<String>> getServers() {
        if(redis == null) return null;
        return redis.getServers();
    }

}
