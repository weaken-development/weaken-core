package life.weaken.core.bungeeapi.redis;

public class RedisConfig {

    private final String host;
    private final int port;

    private final String username;
    private final String password;

    private final String serverName;

    public RedisConfig(String serverName, String host, int port, String username, String password) {
        this.host = host;
        this.port = port;
        this.username = username;
        this.password = password;
        this.serverName = serverName;
    }

    public String getHost() {
        return host;
    }

    public int getPort() {
        return port;
    }

    public String getPassword() {
        return password;
    }

    public String getUsername() {
        return username;
    }

    public String getServerName() {
        return serverName;
    }
}
