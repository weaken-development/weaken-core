package life.weaken.core.bungeeapi.redis;

import org.bukkit.Bukkit;
import org.bukkit.plugin.Plugin;
import org.bukkit.scheduler.BukkitRunnable;
import org.bukkit.scheduler.BukkitTask;
import redis.clients.jedis.Jedis;
import redis.clients.jedis.JedisPool;
import redis.clients.jedis.JedisPoolConfig;
import redis.clients.jedis.Pipeline;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.concurrent.CompletableFuture;

public class Redis {

    private final String serverName;
    private final JedisPool jedisPool;
    private final BukkitTask updateTask;
    private final Plugin plugin;

    public Redis(Plugin plugin, RedisConfig config) {
        this.plugin = plugin;
        JedisPoolConfig poolConfig = new JedisPoolConfig();
        poolConfig.setMinIdle(8);
        poolConfig.setMaxTotal(32);
        jedisPool = new JedisPool(poolConfig, config.getHost(), config.getPort(), config.getUsername(), config.getPassword());

        this.serverName = config.getServerName();

        updateTask = new BukkitRunnable() {
            @Override
            public void run() {
                updateOwnInformation();
            }
        }.runTaskTimerAsynchronously(plugin, 0L, 20L);

    }

    private String getStatus() {
        if(Bukkit.hasWhitelist()) return "WHITELISTED";
        if(Bukkit.getOnlinePlayers().size() >= Bukkit.getMaxPlayers()) return "FULL";
        return "ONLINE";
    }

    public void updateOwnInformation() {
        try(Jedis jedis = jedisPool.getResource()) {
            Pipeline pipeline = jedis.pipelined();
            pipeline.set(serverName + "-LastUpdate", "" + System.currentTimeMillis());
            pipeline.set(serverName + "-MaxPlayerCount", "" + Bukkit.getMaxPlayers());
            pipeline.set(serverName + "-PlayerCount", "" + Bukkit.getOnlinePlayers().size());
            pipeline.set(serverName + "-Status", getStatus());
            pipeline.sync();
        }
    }

    public void shutdown() {
        updateTask.cancel();
        updateOwnInformation();
        try(Jedis jedis = jedisPool.getResource()) {
            jedis.set(serverName + "-Status", "OFFLINE");
            jedis.set(serverName + "-PlayerCount", "0");
        }
        jedisPool.close();
    }

    public CompletableFuture<Long> getLastUpdate(String server) {
        CompletableFuture<Long> future = new CompletableFuture<>();

        new BukkitRunnable() {
            @Override
            public void run() {
                try(Jedis jedis = jedisPool.getResource()) {
                    String i = jedis.get(server + "-LastUpdate");
                    future.complete(i == null ? 0 : Long.parseLong(i));
                }
            }
        }.runTaskAsynchronously(plugin);

        return future;
    }

    public CompletableFuture<Integer> getMaxPlayerCount(String server) {
        CompletableFuture<Integer> future = new CompletableFuture<>();

        new BukkitRunnable() {
            @Override
            public void run() {
                try(Jedis jedis = jedisPool.getResource()) {
                    String i = jedis.get(server + "-MaxPlayerCount");
                    future.complete(i == null ? 0 : Integer.parseInt(i));
                }
            }
        }.runTaskAsynchronously(plugin);

        return future;
    }

    public CompletableFuture<String> getStatus(String server) {
        CompletableFuture<String> future = new CompletableFuture<>();

        new BukkitRunnable() {
            @Override
            public void run() {
                try(Jedis jedis = jedisPool.getResource()) {
                    String i = jedis.get(server + "-Status");
                    future.complete(i == null ? "OFFLINE" : i);
                }
            }
        }.runTaskAsynchronously(plugin);

        return future;
    }

    public CompletableFuture<Integer> getPlayerCount(String server) {
        CompletableFuture<Integer> future = new CompletableFuture<>();

        new BukkitRunnable() {
            @Override
            public void run() {
                try(Jedis jedis = jedisPool.getResource()) {
                    String i = jedis.get(server + "-PlayerCount");
                    future.complete(i == null ? 0 : Integer.parseInt(i));
                }
            }
        }.runTaskAsynchronously(plugin);

        return future;
    }

    public CompletableFuture<List<String>> getServers() {
        CompletableFuture<List<String>> future = new CompletableFuture<>();

        new BukkitRunnable() {
            @Override
            public void run() {
                try(Jedis jedis = jedisPool.getResource()) {
                    String i = jedis.get("Bungee-Servers");
                    future.complete(i == null ? new ArrayList<>() : Arrays.asList(i.split(", ")));
                }
            }
        }.runTaskAsynchronously(plugin);

        return future;
    }

    public CompletableFuture<Integer> getGlobalPlayerCount() {
        CompletableFuture<Integer> future = new CompletableFuture<>();

        new BukkitRunnable() {
            @Override
            public void run() {
                try(Jedis jedis = jedisPool.getResource()) {
                    String i = jedis.get("Bungee-PlayerCount");
                    future.complete(i == null ? 0 : Integer.parseInt(i));
                }
            }
        }.runTaskAsynchronously(plugin);

        return future;
    }

}
