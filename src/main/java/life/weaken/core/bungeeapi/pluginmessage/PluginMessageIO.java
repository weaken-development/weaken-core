package life.weaken.core.bungeeapi.pluginmessage;

import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.plugin.Plugin;

public class PluginMessageIO {

    public static void sendPluginMessage(Plugin plugin, Player player, byte[] message) {
        player.sendPluginMessage(plugin, "BungeeCord", message);
    }

    public static void sendPluginMessage(Plugin plugin, byte[] message) {
        Bukkit.getServer().sendPluginMessage(plugin, "BungeeCord", message);
    }

}

