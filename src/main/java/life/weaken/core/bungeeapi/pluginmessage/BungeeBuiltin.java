package life.weaken.core.bungeeapi.pluginmessage;

import com.google.common.io.ByteArrayDataInput;
import com.google.common.io.ByteStreams;
import life.weaken.core.utils.PluginMessageBuilder;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.plugin.Plugin;

import java.util.*;
import java.util.concurrent.*;

public class BungeeBuiltin {

    private final Map<String, List<CompletableFuture<?>>> callbackMap = new HashMap<>();

    private final Plugin plugin;

    public BungeeBuiltin(Plugin plugin) {
        this.plugin = plugin;

        Bukkit.getServer().getMessenger().registerIncomingPluginChannel(plugin, "BungeeCord", this::onPluginMessageReceived);
        Bukkit.getServer().getMessenger().registerOutgoingPluginChannel(plugin, "BungeeCord");
    }

    public void shutdown() {
        Bukkit.getServer().getMessenger().unregisterIncomingPluginChannel(plugin);
        Bukkit.getServer().getMessenger().unregisterOutgoingPluginChannel(plugin);
    }

    private List<CompletableFuture<?>> getCallbackList(String id) {
        return callbackMap.computeIfAbsent(id, k -> new ArrayList<>());
    }

    @SuppressWarnings("unchecked")
    protected <K> void resolveCallbacks(String id, K value) {
        List<CompletableFuture<?>> callbacks = getCallbackList(id);
        if(callbacks.isEmpty()) return;
        for(CompletableFuture<?> future : callbacks) {
            ((CompletableFuture<K>) future).complete(value);
        }
        callbacks.clear();
    }

    protected void registerCallback(String id, CompletableFuture<?> future) {
        getCallbackList(id).add(future);
    }

    @SuppressWarnings("UnstableApiUsage")
    public void onPluginMessageReceived(String channel, Player player, byte[] inputraw) {
        ByteArrayDataInput input = ByteStreams.newDataInput(inputraw);
        String subChannel = input.readUTF();

        if(subChannel.equalsIgnoreCase("IP") || subChannel.equalsIgnoreCase("IPOther")) {
            String username = input.readUTF();
            String address = input.readUTF();
            resolveCallbacks(subChannel + "-" + username, address);
        }
        else if(subChannel.equalsIgnoreCase("PlayerCount")) {
            String server = input.readUTF();
            int playerCount = input.readInt();
            resolveCallbacks(subChannel + "-" + server, playerCount);
        }
        else if(subChannel.equalsIgnoreCase("PlayerList")) {
            String server = input.readUTF();
            String playerList = input.readUTF();
            resolveCallbacks(subChannel + "-" + server, Arrays.asList(playerList.split(", ")));
        }
        else if(subChannel.equalsIgnoreCase("GetServers")) {
            String serverList = input.readUTF();
            resolveCallbacks(subChannel, Arrays.asList(serverList.split(", ")));
        }
        else if(subChannel.equalsIgnoreCase("GetServer")) {
            String serverName = input.readUTF();
            resolveCallbacks(subChannel, serverName);
        }
        else if(subChannel.equalsIgnoreCase("UUID") || subChannel.equalsIgnoreCase("UUIDOther")) {
            String username = input.readUTF();
            String uuid = input.readUTF();
            resolveCallbacks(subChannel + "-" + username, uuid);
        }
    }

    public void connect(Player player, String server) {
        PluginMessageIO.sendPluginMessage(plugin, player, new PluginMessageBuilder()
                .write("Connect")
                .write(server)
                .toByteArray());
    }

    public void connectOther(String player, String server) {
        PluginMessageIO.sendPluginMessage(plugin, new PluginMessageBuilder()
                .write("ConnectOther")
                .write(player)
                .write(server)
                .toByteArray());
    }

    public CompletableFuture<String> ip(Player player) {
        CompletableFuture<String> future = new CompletableFuture<>();
        registerCallback("IP-" + player.getName(), future);

        PluginMessageIO.sendPluginMessage(plugin, new PluginMessageBuilder()
                .write("IP")
                .toByteArray());

        return future;
    }

    public CompletableFuture<String> ipOther(String player) {
        CompletableFuture<String> future = new CompletableFuture<>();
        registerCallback("IPOther-" + player, future);

        PluginMessageIO.sendPluginMessage(plugin, new PluginMessageBuilder()
                .write("IPOther")
                .write(player)
                .toByteArray());

        return future;
    }

    public CompletableFuture<Integer> playerCount() {
        return playerCount("ALL");
    }

    public CompletableFuture<Integer> playerCount(String server) {
        CompletableFuture<Integer> future = new CompletableFuture<>();
        registerCallback("PlayerCount-" + server, future);

        PluginMessageIO.sendPluginMessage(plugin, new PluginMessageBuilder()
                .write("PlayerCount")
                .write(server)
                .toByteArray());

        return future;
    }

    public CompletableFuture<List<String>> playerList() {
        return playerList("ALL");
    }

    public CompletableFuture<List<String>> playerList(String server) {
        CompletableFuture<List<String>> future = new CompletableFuture<>();
        registerCallback("PlayerList-" + server, future);

        PluginMessageIO.sendPluginMessage(plugin, new PluginMessageBuilder()
                .write("PlayerList")
                .write(server)
                .toByteArray());

        return future;
    }

    public CompletableFuture<List<String>> getServers() {
        CompletableFuture<List<String>> future = new CompletableFuture<>();
        registerCallback("GetServers", future);

        PluginMessageIO.sendPluginMessage(plugin, new PluginMessageBuilder()
                .write("GetServers")
                .toByteArray());

        return future;
    }

    public void message(String message) {
        message("ALL", message);
    }

    public void message(String player, String message) {
        PluginMessageIO.sendPluginMessage(plugin, new PluginMessageBuilder()
                .write("Message")
                .write(player)
                .write(message)
                .toByteArray());
    }

    public void messageRaw(String message) {
        messageRaw("ALL", message);
    }

    public void messageRaw(String player, String message) {
        PluginMessageIO.sendPluginMessage(plugin, new PluginMessageBuilder()
                .write("MessageRaw")
                .write(player)
                .write(message)
                .toByteArray());
    }

    public CompletableFuture<String> getServer() {
        CompletableFuture<String> future = new CompletableFuture<>();
        registerCallback("GetServer", future);

        PluginMessageIO.sendPluginMessage(plugin, new PluginMessageBuilder()
                .write("GetServer")
                .toByteArray());

        return future;
    }

    public CompletableFuture<String> uuid(Player player) {
        CompletableFuture<String> future = new CompletableFuture<>();
        registerCallback("UUID-" + player.getName(), future);

        PluginMessageIO.sendPluginMessage(plugin, new PluginMessageBuilder()
                .write("UUID")
                .toByteArray());

        return future;
    }

    public CompletableFuture<String> uuidOther(String player) {
        CompletableFuture<String> future = new CompletableFuture<>();
        registerCallback("UUIDOther-" + player, future);

        PluginMessageIO.sendPluginMessage(plugin, new PluginMessageBuilder()
                .write("UUIDOther")
                .write(player)
                .toByteArray());

        return future;
    }

    public void kickPlayer(String player, String reason) {
        PluginMessageIO.sendPluginMessage(plugin, new PluginMessageBuilder()
                .write("KickPlayer")
                .write(player)
                .write(reason)
                .toByteArray());
    }

}
