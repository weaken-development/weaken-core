package life.weaken.core.gui.items;

import life.weaken.core.gui.guis.Gui;
import org.bukkit.Bukkit;
import org.bukkit.event.inventory.InventoryCloseEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.plugin.Plugin;
import org.bukkit.scheduler.BukkitTask;

public class UpdatingItem extends GuiItem {

    private final BukkitTask updateTask;

    public UpdatingItem(Gui gui, ItemStack itemStack, Plugin plugin, Long updateDelay) {
        super(gui, itemStack);
        updateTask = Bukkit.getScheduler().runTaskTimer(plugin, this::update, updateDelay, updateDelay);
    }

    public void update() {

    }

    @Override
    public void close(InventoryCloseEvent event) {
        updateTask.cancel();
    }

}
