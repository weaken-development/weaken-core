package life.weaken.core.gui.items;

import life.weaken.core.gui.guis.Gui;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.event.inventory.InventoryCloseEvent;
import org.bukkit.inventory.ItemStack;

public class GuiItem {

    private final ItemStack itemStack;
    private final Gui gui;

    public GuiItem(Gui gui, ItemStack itemStack) {
        this.itemStack = itemStack;
        this.gui = gui;
    }

    public void click(InventoryClickEvent event) {
        event.setCancelled(true);
    }

    public void close(InventoryCloseEvent event) {

    }

    public ItemStack getItemStack() {
        return itemStack;
    }

    public Gui getGui() {
        return gui;
    }

}
