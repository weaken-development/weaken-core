package life.weaken.core.gui;

import life.weaken.core.gui.guis.Gui;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.event.inventory.InventoryCloseEvent;

public record GuiListener(GuiManager manager) implements Listener {

    @EventHandler
    public void onInventoryClick(InventoryClickEvent event) {
        if(!(event.getWhoClicked() instanceof Player player)) return;
        Gui gui = manager.getGui(player);
        if(gui != null) gui.click(event);
    }

    @EventHandler
    public void onInventoryClose(InventoryCloseEvent event) {
        if(!(event.getPlayer() instanceof Player player)) return;
        Gui gui = manager.getGui(player);
        if(gui == null) return;
        if(gui.getInventory() != event.getInventory()) return;
        gui.close(event);
        manager.closeGui(player);
    }


}
