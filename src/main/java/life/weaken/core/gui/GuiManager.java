package life.weaken.core.gui;

import life.weaken.core.gui.guis.Gui;
import org.bukkit.entity.Player;
import org.bukkit.plugin.Plugin;

import java.util.HashMap;

public class GuiManager {

    private final Plugin plugin;
    private final HashMap<Player, Gui> guiMap;

    public GuiManager(Plugin plugin) {
        guiMap = new HashMap<>();
        this.plugin = plugin;
    }

    public void showGui(Player player, Gui gui) {
        guiMap.remove(player);
        guiMap.put(player, gui);
        gui.show(player);
    }

    public void closeGui(Player player) {
        guiMap.remove(player);
    }

    public Gui getGui(Player player) {
        return guiMap.get(player);
    }

    public void shutdown() {
        for(Player player : guiMap.keySet()) {
            player.closeInventory();
        }
    }

    public Plugin getPlugin() {
        return plugin;
    }
}
