package life.weaken.core.gui.guis;

import life.weaken.core.gui.GuiManager;
import life.weaken.core.gui.items.GuiItem;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.event.inventory.InventoryCloseEvent;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;

import java.util.HashMap;

public class Gui {

    private final Inventory inventory;
    private final HashMap<Integer, GuiItem> items;
    private final int size;
    protected final GuiManager guiManager;
    private Player viewer;

    public Gui(GuiManager manager, String name, int rows) {
        this.size = rows * 9;
        inventory = Bukkit.getServer().createInventory(null, size, name);
        items = new HashMap<>();
        this.guiManager = manager;
    }

    public void addItem(int slot, GuiItem item) {
        items.put(slot, item);
        inventory.setItem(slot, item.getItemStack());
    }

    public void show(Player player) {
        this.viewer = player;
        player.closeInventory();
        player.openInventory(inventory);
    }

    public void close(InventoryCloseEvent event) {
        for(GuiItem item : items.values()) item.close(event);
    }

    public void click(InventoryClickEvent event) {
        event.setCancelled(true);
        GuiItem item = items.get(event.getRawSlot());
        if(item != null) item.click(event);
    }

    public void fillRemaining(ItemStack itemStack) {
        for(int i = 0; i < size; i++) {
            if(!items.containsKey(i)) addItem(i, new GuiItem(this, itemStack) {
                @Override
                public void click(InventoryClickEvent event) {
                    event.setCancelled(true);
                }
            });
        }
    }

    public Inventory getInventory() {
        return inventory;
    }

    public Player getViewer() {
        return viewer;
    }

    public GuiManager getGuiManager() {
        return guiManager;
    }
}
