package life.weaken.core.gui.guis;


import life.weaken.core.gui.GuiManager;
import life.weaken.core.gui.items.GuiItem;
import org.bukkit.entity.Player;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.inventory.ItemStack;

public class SubGui extends Gui {

    private final Gui parent;

    public SubGui(GuiManager manager, Gui parent, String name, int rows) {
        super(manager, name, rows);
        this.parent = parent;
    }

    public GuiItem generateBackButton(ItemStack itemStack) {
        return new GuiItem(this, itemStack) {
            @Override
            public void click(InventoryClickEvent event) {
                super.click(event);
                guiManager.showGui((Player) event.getWhoClicked(), parent);
            }
        };
    }


}
